import java.util.Arrays;

public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      Color[] test = {Color.red, Color.blue, Color.red, Color.green};
      System.out.println(Color.blue.ordinal());
      System.out.println(Arrays.toString(test));
      reorder(test);
      Color[] test2 = new Color[] {Color.red};
      reorder(test2);
      System.out.println(Arrays.toString(test2));
   }

   public static void reorder(Color[] balls) {
      int start = 0;
      int end = balls.length - 1;

      for (int i = 0; i < end + 1; i++) {
         Color current = balls[i];
         switch (current) {
            case red:
               balls[i] = balls[start];
               balls[start] = current;
               start++;
               break;
            case blue:
               balls[i] = balls[end];
               balls[end] = current;
               end--;
               i--;
               break;
         }
      }
   }
}

